<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Daftarcontroller extends Controller
{
    public function daftar(){
        return view('page.form');
    }
    public function submit(Request $request){
        $namadepan = $request['fnama'];
        $namabelakang = $request['lnama'];
        return view('page.selamat'. compact('namadepan', 'namabelakang'));
    }
}
